if RequiredScript == "lib/tweak_data/weaponfactorytweakdata" then

	local old_wftd_init = WeaponFactoryTweakData.init
	function WeaponFactoryTweakData:init()
		old_wftd_init(self)
		self:stuff_general()
	end
		
	function WeaponFactoryTweakData:stuff_general()

table.list_append(self.wpn_fps_ass_amcar.uses_parts, {
"wpn_fps_m4_uupg_fg_lr300", "wpn_fps_upg_ass_m4_fg_lvoa", "wpn_fps_upg_ass_m4_fg_moe", "wpn_fps_upg_fg_jp", "wpn_fps_upg_fg_smr", "wpn_fps_upg_m4_m_pmag", "wpn_fps_ass_l85a2_m_emag", "wpn_fps_upg_m4_g_ergo", "wpn_fps_upg_m4_g_sniper", "wpn_fps_upg_m4_g_mgrip", "wpn_fps_upg_m4_g_hgrip", "wpn_fps_upg_m4_s_pts"
})

	






-- BARRELS


self.parts.wpn_fps_ass_g3_b_short.stats = {value = 2, spread = -2, recoil = 2, concealment = 2}
self.parts.wpn_fps_upg_ak_b_draco.stats = {value = 2, damage = 6, extra_ammo = 0, total_ammo_mod = 0, spread = -2, recoil = 0, concealment = 1}
self.parts.wpn_fps_pis_c96_b_long.stats = {value = 2, damage = 30, extra_ammo = 0, total_ammo_mod = -5, spread = 2, recoil = 0, concealment = -6}
self.parts.wpn_fps_lmg_hk21_b_long.stats = {damage = 2, spread = 1, recoil = -2, concealment = -1}
self.parts.wpn_fps_upg_g36_fg_long.stats = {damage = 1, spread = 2, recoil = -2, concealment = -3}
self.parts.wpn_fps_lmg_rpk_fg_standard.stats = {spread = 2, recoil = -1, concealment = 1}
self.parts.wpn_fps_lmg_m249_b_long.stats = {spread = 1, spread = 1, recoil = -1, concealment = -1}
self.parts.wpn_fps_lmg_m249_fg_mk46.stats = {recoil = 1, concealment = 1}
self.parts.wpn_fps_ass_famas_b_sniper.stats = {damage = 3, recoil = -3, spread = 4, concealment = -4}
self.parts.wpn_fps_ass_famas_b_long.stats = {damage = 1, recoil = -1, spread = 1, concealment = -2}
self.parts.wpn_fps_ass_famas_b_short.stats = {recoil = 3, spread = -2, concealment = 3}
self.parts.wpn_fps_ass_scar_b_long.stats = {spread = 3, recoil = -3, damage = 1, concealment = -2}
self.parts.wpn_fps_snp_winchester_b_long.stats = {spread = 2, damage = 2, concealment = -3}
self.parts.wpn_fps_sho_ksg_b_long.stats = {spread = 3, recoil = -3, damage = 1, concealment = -2, extra_ammo = 1}
self.parts.wpn_fps_ass_fal_fg_04.stats = {spread = 3, recoil = -3, damage = 2, concealment = -3}
self.parts.wpn_fps_ass_fal_fg_01.stats = {spread = -2, recoil = 2, concealment = 6}
self.parts.wpn_fps_ass_galil_fg_sniper.stats = {spread = 3, recoil = -4, damage = 3, conceal,emt = -2}
self.parts.wpn_fps_ass_galil_fg_mar.stats = {damage = 2, recoil = 2, spread = -2, concealment = 4}
self.parts.wpn_fps_smg_p90_b_long.stats = {damage = 2, recoil = -1, spread = 1, concealment = -2}
self.parts.wpn_fps_m4_uupg_b_long.stats = {damage = 2, recoil = -2, spread = 2, concealment = -3}
self.parts.wpn_fps_aug_b_long.stats = {damage = 2, recoil = -1, spread = 1, concealment = -2}
self.parts.wpn_fps_sho_boot_b_long.stats = {damage = 1, recoil = -2, spread = 2, concealment = -2, extra_ammo = 1}
self.parts.wpn_fps_sho_boot_b_short.stats = {recoil = 3, spread = -2, concealment = 3, extra_ammo = -1}
self.parts.wpn_fps_lmg_m134_barrel_extreme.stats = {recoil = -3, spread = 3, damage = 5, concealment = -5}
self.parts.wpn_fps_ass_ak5_b_short.stats = {recoil = 2, spread = -2, damage = 1, concealment = 2}


--Foregrip
self.parts.wpn_fps_upg_fg_jp.stats = {spread = 4, recoil = -2, concealment = 2}
self.parts.wpn_fps_upg_fg_smr.stats = {spread = 3, recoil = 1, concealment = -1}
self.parts.wpn_fps_upg_ass_m4_fg_lvoa.stats = {spread = 1, recoil = 4}
self.parts.wpn_fps_upg_ass_m4_fg_moe.stats = {spread = 2, recoil = 2, concealment = 2}
self.parts.wpn_fps_smg_thompson_foregrip_discrete.stats = {recoil = 1}
self.parts.wpn_fps_ass_ak5_fg_ak5c.stats = {spread = 1, recoil = 2, concealment = -2}
self.parts.wpn_fps_ass_ak5_fg_fnc.stats = {spread = 2, recoil = -2, concealment = 1}


--Stock
self.parts.wpn_fps_smg_thompson_stock_discrete.stats = {recoil = 2}






--Grip
self.parts.wpn_fps_smg_thompson_grip_discrete.stats = {spread = 1}


--Barrel_ext
self.parts.wpn_fps_upg_ns_ass_smg_small.stats = {spread = 1, suppression = 6, concealment = -2, alert_size = 12}
self.parts.wpn_fps_upg_ns_ass_smg_medium.stats = {spread = 1, recoil = 1, suppression = 5, concealment = -4, alert_size = 12}
self.parts.wpn_fps_upg_ns_ass_smg_large.stats = {spread = 2, recoil = 2, concealment = -5, suppression = 3, alert_size = 12}
self.parts.wpn_fps_upg_ns_shot_thick.stats = {spread = 1, suppression = 4, concealment = -2, alert_size = 12}
self.parts.wpn_fps_upg_ns_sho_salvo_large.stats = {spread = 1, recoil = 2, suppression = 4, concealment = -3, alert_size = 12}
self.parts.wpn_fps_sho_aa12_barrel_silenced.stats = {recoil = 2, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_model70_ns_suppressor.stats = {recoil = 2, spread = 1, suppression = 6, concealment = -4, alert_size = 12}
self.parts.wpn_fps_snp_msr_ns_suppressor.stats = {recoil = 2, spread = 1, suppression = 5, concealment = -3, alert_size = 12}
self.parts.wpn_fps_snp_wa2000_b_suppressed.stats = {recoil = 2, spread = 1, suppression = 4, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_desertfox_b_silencer.stats = {recoil = 3, spread = 2, suppression = 5, concealment = -3, alert_size = 12}
self.parts.wpn_fps_snp_tti_ns_hex.stats = {recoil = 2, spread = 1, suppression = 3, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_r93_b_suppressed.stats = {recoil = 2, spread = 1, suppression = 5, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_winchester_b_suppressed.stats = {recoil = 1, spread = 2, suppression = 2, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_siltstone_b_silenced.stats = {recoil = 2, spread = 1, suppression = 3, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_mosin_b_sniper.stats = {recoil = 4, spread = 1, suppression = 7, concealment = -2, alert_size = 12}
self.parts.wpn_fps_snp_m95_barrel_suppressed.stats = {recoil = 3, spread = 2, suppression = 12, concealment = -3, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_medium_gem.stats = {recoil = 2, spread = 1, suppression = 2, concealment = -2, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_large_kac.stats = {recoil = 1, spread = 3, suppression = 4, concealment = -3, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_medium.stats = {recoil = 1, spread = 1, suppression = 6, concealment = -2, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_small.stats = {recoil = 1, concealment = -1, suppression = 8, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_large.stats = {recoil = 2, spread = 2, concealment = -3, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_medium_slim.stats = {recoil = 1, spread = 3, concealment = -3, alert_size = 12}
self.parts.wpn_fps_upg_ns_ass_filter.stats = {damage = -3, spread = -2, recoil = 2, concealment = -3, alert_size = 12}
self.parts.wpn_fps_upg_ns_pis_jungle.stats = {recoil = 2, spread = 2, concealment = -3, alert_size = 12}
self.parts.wpn_fps_smg_cobray_ns_silencer.stats = {recoil = 2, spread = 1, concealment = -2, suppression = 5, alert_size = 12}
self.parts.wpn_fps_smg_mp5_fg_mp5sd.stats = {recoil = 3, spread = 1, concealment = -2, suppression = 5, alert_size = 12}
self.parts.wpn_fps_smg_sr2_ns_silencer.stats = {recoil = 2, spread = 1, concealment = -2, suppression = 3, alert_size = 12}
self.parts.wpn_fps_smg_p90_b_ninja.stats = {recoil = 2, spreada = 2, concealment = -2, alert_size = 12}
self.parts.wpn_fps_smg_mp7_b_suppressed.stats = {recoil = 2, spread = 1, concealment = -2, alert_size = 12}
self.parts.wpn_fps_smg_baka_b_midsupp.stats = {recoil = 3, concealment = -2, alert_size = 12}
self.parts.wpn_fps_smg_baka_b_smallsupp.stats = {recoil = 2, spread = 1, suppression = 5, concealment = -1, alert_size = 12}
self.parts.wpn_fps_smg_baka_b_longsupp.stats = {recoil = 4, spread = 2, suppression = 6, concealment = -3, alert_size = 12}
self.parts.wpn_fps_smg_polymer_ns_silencer.stats = {recoil = 2, spread = 2, suppression = 4, concealment = -3, alert_size = 12}
self.parts.wpn_fps_smg_sterling_b_suppressed.stats = {recoil = 2, spread = 1, concealment = -2, suppression = 2, alert_size = 12}
self.parts.wpn_fps_smg_uzi_b_suppressed.stats = {recoil = 3, concealment = -2, suppression = 3, alert_size = 12}
self.parts.wpn_fps_sho_rota_b_silencer.stats = {recoil = 2, concealment = -1, alert_size = 12}
self.parts.wpn_fps_sho_striker_b_suppressed.stats = {alert_size = 12, recoil = 3, spread = 1, concealment = -3, suppression = 5}
self.parts.wpn_fps_smg_schakal_ns_silencer.stats = {alert_size = 12, recoil = 4, spread = 2, concealment = -2, suppression = 5}

-- Magazine
self.parts.wpn_fps_smg_mac10_m_extended.stats = {extra_ammo = 5, total_ammo_mod = 0, spread = 0, recoil = 2, concealment = -1}
self.parts.wpn_fps_m4_uupg_m_std.stats = {extra_ammo = 5, total_ammo_mod = 0, spread = 0, recoil = 0, concealment = -1}
self.parts.wpn_fps_upg_ak_m_uspalm.stats = {extra_ammo = 0, recoil = 1, concealment = -1}
self.parts.wpn_fps_m4_upg_m_quick.stats = {extra_ammo = 5, total_ammo_mod = 0, spread = 0, recoil = 0, concealment = -2, reload = 5}
self.parts.wpn_fps_upg_m4_m_pmag.stats = {extra_ammo = 5, total_ammo_mod = 0, spread = 2, recoil = 0, concealment = -1}
self.parts.wpn_fps_ass_l85a2_m_emag.stats = {extra_ammo = 5, concealment = -1}
self.parts.wpn_fps_upg_m4_m_l5.stats = {extra_ammo = 5, concealment = -1}
self.parts.wpn_fps_smg_mp5_m_straight.stats = {damage = 20, total_ammo_mod = -10, spread = -3, recoil = -10}
self.parts.wpn_fps_upg_m4_m_straight.stats = {extra_ammo = -5, concealment = 2}
self.parts.wpn_fps_smg_mp7_m_extended.stats = {extra_ammo = 10, concealment = -2}
self.wpn_fps_ass_m4.override = {}
self.wpn_fps_ass_m4.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_m4.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_ass_m4.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_m4.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_x_mac10.override.wpn_fps_smg_mac10_m_extended = { stats = { extra_ammo = 10, total_ammo_mod = 0, spread = 0, recoil = 2, concealment = -1 } }
self.wpn_fps_ass_l85a2.override = {}
self.wpn_fps_ass_l85a2.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_l85a2.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_ass_l85a2.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_l85a2.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_corgi.override = {}
self.wpn_fps_ass_corgi.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_corgi.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_ass_corgi.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_corgi.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_hajk.override = {}
self.wpn_fps_smg_hajk.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_hajk.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_hajk.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_hajk.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_smg_x_hajk.override = {}
self.wpn_fps_smg_x_hajk.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_x_hajk.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_x_hajk.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_smg_x_hajk.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_smg_x_hajk.override.wpn_fps_upg_m4_m_straight = { stats = { extra_ammo = -10, total_ammo_mod = 0, recoil = 0, concealment = 2 } }
self.wpn_fps_ass_amcar.override = {}
self.wpn_fps_ass_amcar.override.wpn_fps_upg_m4_m_quad = { stats = { extra_ammo = 20, total_ammo_mod = 0, recoil = 1, concealment = -4 } }
self.wpn_fps_ass_m16.override = {}
self.wpn_fps_ass_m16.override.wpn_fps_upg_m4_m_quad = { stats = { extra_ammo = 20, total_ammo_mod = 0, recoil = 1, concealment = -4 } }
self.wpn_fps_ass_ak5.override = {}
self.wpn_fps_ass_ak5.override.wpn_fps_upg_m4_m_pmag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_ak5.override.wpn_fps_m4_upg_m_quick = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 0, concealment = -2, reload = 5 } }
self.wpn_fps_ass_ak5.override.wpn_fps_ass_l85a2_m_emag = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.wpn_fps_ass_ak5.override.wpn_fps_upg_m4_m_l5 = { stats = { extra_ammo = 0, total_ammo_mod = 0, recoil = 1, concealment = -1 } }
self.parts.wpn_fps_smg_shepheard_mag_extended.stats = {extra_ammo = 5, concealment = -2}
self.wpn_fps_smg_x_shepheard.override.wpn_fps_smg_shepheard_mag_extended = { stats = { extra_ammo = 10, total_ammo_mod = 0, spread = 0, recoil = 0, concealment = -3 } }
self.wpn_fps_smg_x_mp7.override.wpn_fps_smg_mp7_m_extended = { stats = { extra_ammo = 20, total_ammo_mod = 0, spread = 0, recoil = 2, concealment = -1 } }

-- Scope
self.parts.wpn_fps_upg_o_t1micro.stats = {spread = 1, concealment = -1, zoom = 3}
self.parts.wpn_fps_upg_o_docter.stats = {spread = 1, concealment = -1, zoom = 2}
self.parts.wpn_fps_upg_o_cmore.stats = {spread = 2, concealment = -2, zoom = 3}
self.parts.wpn_fps_upg_o_cs.stats = {spread = 2, concealment = -2, zoom = 3}
self.parts.wpn_fps_upg_o_reflex.stats = {concealment = -1, zoom = 3}
self.parts.wpn_fps_upg_o_rx01.stats = {spread = 2, concealment = -2, zoom = 4}
self.parts.wpn_fps_upg_o_eotech.stats = {spread = 2, concealment = -2, zoom = 3}
self.parts.wpn_fps_upg_o_eotech_xps.stats = {spread = 1, concealment = -1, zoom = 3}
self.parts.wpn_fps_upg_o_rx30.stats = {spread = 2, concealment = -2, zoom = 4}
self.parts.wpn_fps_upg_o_aimpoint_2.stats = {spread = 2, concealment = -2, zoom = 4}
self.parts.wpn_fps_upg_o_specter.stats = {spread = 3, concealment = -4, zoom = 4}
self.parts.wpn_fps_upg_o_acog.stats = {spread = 3, concealment = -4, zoom = 5}
self.parts.wpn_fps_upg_o_spot.stats = {spread = 3, concealment = -5, zoom = 4}
self.parts.wpn_fps_upg_o_rmr.stats = {spread = 1, concealment = -1, zoom = 3}
self.parts.wpn_fps_upg_o_leupold.stats = {spread = 4, concealment = -6, zoom = 10}
self.parts.wpn_fps_upg_o_box.stats = {spread = 3, concealment = -5, zoom = 10}
self.parts.wpn_fps_upg_o_aimpoint.stats = {spread = 2, concealment = -2, zoom = 4}

end

end
