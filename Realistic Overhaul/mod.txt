{
    "name" : "Tactical Realism Weapon Mod",
    "description" : "A realistic weapons mod based on Blaze Rebalance, all stats are diff by weapons'real life counterparts",
    "author" : "ctbear",
    "contact" : "no",
    "version" : "5.4"
    "blt_version" : 2,

"updates" : [
    {
        "revision" : 74,
        "identifier" : "RealisticRebalance",
    }
]
	"hooks": [
		{"hook_id" : "lib/tweak_data/weaponfactorytweakdata","script_path" : "Attachments.lua"},
		{"hook_id" : "lib/tweak_data/weapontweakdata","script_path" : "Rebalance_weapons.lua"}
		]
}